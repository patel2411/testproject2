const request = require('request');

exports.getStudents = function() {
  return new Promise(function(resolve, reject) {
    const options = {
      url: 'https://www.hatchways.io/api/assessment/students'
    };
    request(options, function(error, response, body) {
      if (error) reject(new Error('Request to get page failed!'));
      resolve(body);
    });
  });
};
