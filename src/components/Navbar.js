import React, { PureComponent } from 'react';
import { Grid, Menu, Icon, Header } from 'semantic-ui-react';
class Navbar extends PureComponent {
  render() {
    return (
      <div>
        <Grid columns={1}>
          <Grid.Column>
            <Menu inverted widths={1}>
              <Menu.Item>
                <Header inverted as="h2">
                  <Icon name="address book" />
                  <Header.Content>Test Project</Header.Content>
                </Header>
              </Menu.Item>
            </Menu>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default Navbar;
