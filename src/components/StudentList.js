import React, { Component, Fragment } from 'react';
import Student from './Student';
import { getStudents } from '../util/getStudentData';
import { Segment, Input, Container } from 'semantic-ui-react';

class StudentList extends Component {
  constructor() {
    super();
    this.state = {
      students: [],
      studentFilter: '',
      tagsFilter: '',
      tagSearch: false,
      tagsDict: {},
      f: false
    };

    getStudents().then(
      r => {
        const s = JSON.parse(r)['students'];
        this.setState({
          students: s,
          f: true
        });
      },
      e => {
        console.log(e);
      }
    );
  }

  handleSearch = e => {
    this.setState({
      studentFilter: e.target.value
    });
  };

  handleTagSearch = e => {
    this.setState({
      tagsFilter: e.target.value
    });
  };

  showTagSearch = s => {
    this.setState({
      tagSearch: s
    });
  };

  addTags = (name, tag) => {
    let tagsDict = this.state.tagsDict;
    let currentTags = tagsDict[name];
    if (currentTags !== undefined) currentTags.push(tag);
    else tagsDict[name] = [tag];
    this.setState({ tagsDict });
  };

  render() {
    let filteredStudents = this.state.students
      .filter(s => {
        let name = s.firstName.toLowerCase() + s.lastName.toLowerCase();
        return name.indexOf(this.state.studentFilter.toLowerCase()) !== -1;
      })
      .filter(s => {
        if (this.state.tagsFilter !== '') {
          let name = s.firstName.toLowerCase() + s.lastName.toLowerCase();
          let tags = this.state.tagsDict[name];
          if (tags !== undefined) {
            return tags.indexOf(this.state.tagsFilter.toLowerCase()) !== -1;
          }
          return false;
        }
        return true;
      })
      .map((s, index) => {
        let name = s.firstName.toLowerCase() + s.lastName.toLowerCase();
        return (
          <Student
            key={index}
            details={s}
            tags={
              this.state.tagsDict[name] !== undefined
                ? this.state.tagsDict[name]
                : []
            }
            addTags={this.addTags}
            showTagSearch={this.showTagSearch}
          />
        );
      });

    return (
      <Fragment>
        <Segment.Group>
          <Segment>
            <Input
              fluid
              icon="search"
              placeholder="Search by name..."
              transparent
              value={this.state.studentFilter}
              onChange={this.handleSearch}
            />
          </Segment>

          {this.state.tagSearch && (
            <Segment>
              <Input
                fluid
                icon="tag"
                placeholder="Search by tags..."
                transparent
                value={this.state.tagsFilter}
                onChange={this.handleTagSearch}
              />
            </Segment>
          )}

          <Container
            style={{
              overflowX: 'hidden',
              overflowY: 'scroll',
              maxHeight: '45em'
            }}
          >
            {this.state.f && filteredStudents}
          </Container>
        </Segment.Group>
      </Fragment>
    );
  }
}

export default StudentList;
