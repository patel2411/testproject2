import React from 'react';
import Navbar from './Navbar';
import StudentList from './StudentList';
import { Grid } from 'semantic-ui-react';

const App = () => {
  return (
    <div>
      <Navbar />
      <Grid centered>
        <Grid.Row columns={3}>
          <Grid.Column width={3} />
          <Grid.Column width={10}>
            <StudentList />
          </Grid.Column>
          <Grid.Column width={3} />
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default App;
