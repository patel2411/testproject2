import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Image,
  Header,
  Button,
  Input,
  Label,
  Divider
} from 'semantic-ui-react';

class Student extends Component {
  constructor() {
    super();
    this.state = {
      show: false,
      buttonIcon: 'plus',
      tagValue: '',
      tags: []
    };
  }

  calculateAverage = grades => {
    let sum = 0;
    let count = grades.length;
    for (let i = 0; i < count; i++) {
      sum = sum + parseFloat(grades[i]);
    }
    return (sum / count).toFixed(2);
  };

  scores = grades => {
    return grades.map((g, index) => {
      return (
        <p key={index}>
          Test {index + 1}: {g}
        </p>
      );
    });
  };

  getTags = tags => {
    return tags.map((tag, index) => {
      return <Label key={index}>{tag}</Label>;
    });
  };

  handleClick = () => {
    this.setState({
      show: this.state.show ? false : true,
      buttonIcon: this.state.buttonIcon === 'plus' ? 'minus' : 'plus'
    });
    this.props.showTagSearch(!this.state.show);
  };

  handleTagChange = e => {
    this.setState({ tagValue: e.target.value.toLowerCase() });
  };

  keyPress = e => {
    if (e.keyCode === 13) {
      this.props.addTags(e.target.name, e.target.value);
      this.setState({
        tagValue: ''
      });
    }
  };

  render() {
    const details = this.props.details;
    const tags = this.props.tags;
    return (
      <Fragment>
        <Grid padded>
          <Grid.Row columns={3}>
            <Grid.Column textAlign="center" width={4}>
              <Image
                src={details.pic}
                size="small"
                centered
                verticalAlign="bottom"
                circular
                bordered
              />
            </Grid.Column>
            <Grid.Column textAlign="left" verticalAlign="bottom" width={10}>
              <Header as="h1">
                {details.firstName} {details.lastName}
              </Header>
              <p>Email: {details.email}</p>
              <p>Company: {details.company}</p>
              <p>Skill: {details.skill}</p>
              <p>Average: {this.calculateAverage(details.grades)} %</p>
              {this.state.show && this.scores(details.grades)}
              {this.state.show && (
                <Grid padded>
                  <Grid.Row>{this.getTags(tags)}</Grid.Row>
                </Grid>
              )}

              {this.state.show && (
                <Input
                  name={
                    details.firstName.toLowerCase() +
                    details.lastName.toLowerCase()
                  }
                  placeholder="Add tags..."
                  value={this.state.tagValue}
                  onChange={this.handleTagChange}
                  onKeyDown={this.keyPress}
                />
              )}
            </Grid.Column>
            <Grid.Column textAlign="right" width={2}>
              <Button
                basic
                type="submit"
                icon={this.state.buttonIcon}
                align="right"
                onClick={this.handleClick}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Divider />
      </Fragment>
    );
  }
}

Student.propTypes = {
  details: PropTypes.object.isRequired,
  addTags: PropTypes.func.isRequired,
  showTagSearch: PropTypes.func.isRequired
};

export default Student;
